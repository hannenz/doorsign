# Doorsign

Docklet for Plank (elementary os dock) to control a raspberry pi based
door sign / traffic light which indicates "Do not disturbed"
It watches for webcam activity (when in a video conference) to trigger
the doorsign as well.

Quite special use case ;-)

On the rasperry pi runs a Server application and communication is done
via UDP / Sockets

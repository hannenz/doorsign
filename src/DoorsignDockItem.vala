using Plank;
using Cairo;
using Linux;

namespace Doorsign {


	public class DoorsignDockItem : DockletItem {

		enum State {
			UNDEFINED, 	// Initial, unknown state
			FREE, 		// "green"
			OCCUPIED 	// "red"
		}
		
		enum CameraState {
			UNDEFINED,
			OFF,
			ON
		}

		const ulong UPDATE_DELAY = 1000000UL; 	// = 1 second

		Gdk.Pixbuf icon_pixbuf_grey;
		Gdk.Pixbuf icon_pixbuf_red;
		Gdk.Pixbuf icon_pixbuf_green;

		DoorsignPreferences prefs;

		bool disposed = false;

		State status = State.UNDEFINED;
		CameraState camstate = CameraState.UNDEFINED;


		public DoorsignDockItem.with_dockitem_file(GLib.File file) {
			GLib.Object(Prefs: new DoorsignPreferences.with_file(file));
		}


		/* Constructor */
		construct {

			// Init Logger (logsto console, Do `killall plank ; sudo plank` in terminal` to see the log
			Logger.initialize("doorsign");
			Logger.DisplayLevel = LogLevel.NOTIFY;

			// Get preferences (Where do we set them?? They are not in gsettings!!
			prefs = (DoorsignPreferences) Prefs;

			// Set Icon and Text (Tooltip);
			Icon = "resource://" + Doorsign.G_RESOURCE_PATH + "/icons/icon_red.png";
			Text = "Control the door sign";

			// Load icons, TODO: Draw the icons with cairo, 
			// like here: https://github.com/ricotz/plank/blob/20c16a0b6a9da41b12524b0208efa13bc6762826/docklets/CPUMonitor/CPUMonitorDockItem.vala#L124
			try {
				icon_pixbuf_grey = new Gdk.Pixbuf.from_resource(Doorsign.G_RESOURCE_PATH + "/icons/icon_grey.png");
				icon_pixbuf_red = new Gdk.Pixbuf.from_resource(Doorsign.G_RESOURCE_PATH + "/icons/icon_red.png");
				icon_pixbuf_green = new Gdk.Pixbuf.from_resource(Doorsign.G_RESOURCE_PATH + "/icons/icon_green.png");
			}
			catch (Error e) {
				warning("Error: " + e.message);
			}


			// Start backup task, which will periodically ask the LED device for
			// its status (LED color) to sync the icon accordingly and also the
			// background tsak will watch for changes in the Webcam becoming
			// in/active
			new Thread<void*> (null, () => {
				while (!disposed) {
					update();
					Thread.usleep(prefs.UpdateInterval * UPDATE_DELAY);
				}
				return null;
			});


			// Initially get the camera status and update the icon
			camstate = get_camera_state();
			update();
		}


		/* Destructor */
		~DoorsignDockItem () {
			disposed = true;
		}


		/**
		 * Periodically check the status of the LED device (Raspi)
		 * Also check for changes of the camera becoming in/active
		 * Update icon according to both 
		 *
		 * @return void
		 */
		protected void update() {

			// Get the LED device's status
			string response = this.send("status");


			// Detect camera status
			var new_camstate = get_camera_state();

			// Only react on CHANGES in camstate
			if (camstate == CameraState.ON && new_camstate == CameraState.OFF) {
				response = this.send("off");
			}
			else if (camstate == CameraState.OFF && new_camstate == CameraState.ON) {
				response = this.send("on");
			}

			camstate = new_camstate;

			// Update icon
			updateIconFromResponse(response);
		}


		/**
		 * Get the current camera state
		 * must be private due to compilation concerns / vala stuff ...
		 *
		 * @return CameraState
		 */
		private CameraState get_camera_state() {
			string std_out, std_err;
			int status;
			try {
				Process.spawn_command_line_sync("fuser /dev/video0", out std_out, out std_err, out status);
				// Logger.notification("fuser /dev/video0 says: %s".printf(std_out));
			}
			catch (GLib.SpawnError e) {
				Logger.notification("Spawning process 'fuser' failed: %s".printf(e.message));
				return CameraState.OFF;
			}
			return (std_out == "") ? CameraState.OFF : CameraState.ON;
		}


		/**
		 * Update the icon according to a string "red" or "green"
		 *
		 * @param string 	The string, either "red" or "green"
		 * @return void
		 */
		protected void updateIconFromResponse(string response) {
			switch (response) {
				case "red":
					status = State.OCCUPIED;
					break;

				case "green":
					status = State.FREE;
					break;

				default:
					status = State.UNDEFINED;
					break;
			}

			// Trigger redrawing the icon
			reset_icon_buffer();
		}



		/**
		 * Draw the icon
		 *
		 * @param Plank.Surface 		Cairo surface to draw upon
		 * @return void
		 */
		protected override void draw_icon(Plank.Surface surface) {

			Cairo.Context ctx = surface.Context;
			Gdk.Pixbuf pb;

			switch (status) {
				case State.OCCUPIED:
					 pb = icon_pixbuf_red.scale_simple(surface.Width, surface.Height, Gdk.InterpType.BILINEAR);
					 break;
				case State.FREE:
					pb = icon_pixbuf_green.scale_simple(surface.Width, surface.Height, Gdk.InterpType.BILINEAR);
					break;

				case State.UNDEFINED:
				default:
					pb = icon_pixbuf_grey.scale_simple(surface.Width, surface.Height, Gdk.InterpType.BILINEAR);
					break;
			}

			Gdk.cairo_set_source_pixbuf(ctx, pb, 0, 0);
			ctx.paint();
		}



		/**
		 * Callback for when the icon gets clicked
		 *
		 * @param PopupButton
		 * @param Gdk.ModifierType
		 * @param uint32
		 * @return AnimationType
		 */
		protected override AnimationType on_clicked(PopupButton button, Gdk.ModifierType mod, uint32 event_time) {

			if (button == PopupButton.LEFT) {
				string message;

				switch (status) {
					case State.OCCUPIED:
						message = "off\n";
						break;

					case State.UNDEFINED:
					case State.FREE:
					default:
						message = "on\n";
						break;
				}

				string response = this.send(message);
				updateIconFromResponse(response);

				return AnimationType.LIGHTEN;
			}
			return AnimationType.NONE;
		}



		/**
		 * Communicate with the LED device: Send a string and returns the
		 * response string
		 *
		 * @param string
		 * @return string
		 */
		protected string send(string message) {

			string responseText = "";

			try {
				InetAddress address = new InetAddress.from_string(prefs.ServerAddress);

				// Connect
				SocketClient client = new SocketClient();
				SocketConnection conn = client.connect(new InetSocketAddress(address, (uint16)prefs.ServerPort));

				conn.output_stream.write(message.data);

				DataInputStream response = new DataInputStream(conn.input_stream);
				responseText = response.read_line();
			}
			catch (Error e) {
				Logger.notification("Doorsign: Failed to connect: %s".printf(e.message));
			}

			return responseText;
		}
	}
}

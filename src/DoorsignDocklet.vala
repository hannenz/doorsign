/**
 * DoorsignDocklet
 *
 * @author Johannes Braun <johannes.braun@hannenz.de>
 * @package doorsign
 * @version 2020-11-13
 */

public static void docklet_init(Plank.DockletManager manager) {
	manager.register_docklet(typeof(Doorsign.DoorsignDocklet));
}

namespace Doorsign {

	/**
	 * Resource path for the icon
	 */
	public const string G_RESOURCE_PATH = "/de/hannenz/doorsign";


	public class DoorsignDocklet : Object, Plank.Docklet {

		public unowned string get_id() {
			return "doorsign";
		}

		public unowned string get_name() {
			return "Doorsign";
		}

		public unowned string get_description() {
			return "Remote controlling a door sign";
		}

		public unowned string get_icon() {
			return "resource://" + Doorsign.G_RESOURCE_PATH + "/icons/icon_red.png";
		}

		public bool is_supported() {
			return false;
		}

		public Plank.DockElement make_element(string launcher, GLib.File file) {
			return new DoorsignDockItem.with_dockitem_file(file);
		}
	}
}

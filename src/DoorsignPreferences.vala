using Plank;

namespace Doorsign {

	public class DoorsignPreferences : DockItemPreferences {

		[Description(nick = "server_address", blurb="IP Adress of server (door sign)")]
		public string ServerAddress { get; set; default = "192.168.178.41"; }

		[Description(nick = "server_port", blurb="Port of server (door sign)")]
		public int ServerPort { get; set; default = 15000; }

		[Description(nick = "update_interval", blurb="Update interval in seconds")]
		public int UpdateInterval { get; set; default = 2; }

		public DoorsignPreferences.with_file(GLib.File file) {
			base.with_file(file);
		}

		protected override void reset_properties() {
			ServerAddress = "192.168.178.41";
			ServerPort = 15000;
			UpdateInterval = 10;
		}
	}
}
